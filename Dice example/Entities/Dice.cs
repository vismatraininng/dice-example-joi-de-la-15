﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dice_example.Entities
{
    public class Dice
    {
        int value;

        public int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }


        public Dice(){
            
        }

        public void Roll() 
        {
            value = RandomValues.NewDiceValue();
        }

        public int X { get; set; }

        public int Y { get; set; }
    }
}
