﻿using Dice_example.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dice_example
{
    public partial class DicesForm : Form
    {
        List<Dice> dices = new List<Dice>();

        public DicesForm()
        {
            InitializeComponent();
        }

        private void testButton_Click(object sender, EventArgs e)
        {
           
            dices = DicesMock(5);
            int i = 0;
            foreach (var dice in dices)
            {
                Console.WriteLine(dice.Value);
                ShowDice(dice,i++);
            }


        }

        private static List<Dice> DicesMock(int numberOfDices)
        {
            List<Dice> dices = new List<Dice>();

            for (int i = 1; i <= numberOfDices; i++)
            {
                Dice dice = new Dice();
                dice.Y = 150+50*((i-1)/5);
                dice.X = 20+((i-1)%5*51);
                dice.Roll();
                dices.Add(dice);
            }

            return dices;
        }

        private void button1_Click(object sender, EventArgs e)
        { 
            HideDices();
            int i = 0;
            foreach (var dice in dices)
            {
                dice.Roll();
                ShowDice(dice,i++);
            }
        }
 
        private void HideDices() // rezolvare de kko
        {
            foreach (var control in this.Controls)
            {
                if (control.GetType() == typeof(Label))
                {
                    ((Label)control).Visible = false;
                }
            }
        }
 
        private void ShowDice(Dice dice, int indexInList)
        {
            Label testLabel = new Label();

            testLabel.BackColor = Color.White;
            testLabel.Font = new Font("Arial", 30, FontStyle.Bold);
            testLabel.BorderStyle = BorderStyle.Fixed3D;
            testLabel.Text = dice.Value.ToString();
            testLabel.Left = dice.X;
            testLabel.Top = dice.Y;
            testLabel.Height = 50;
            testLabel.Width = 50;
            testLabel.ForeColor = Color.Red;

            testLabel.Tag = indexInList.ToString();

            testLabel.Click += new EventHandler(ShowTheNewDice);


            this.Controls.Add(testLabel);
        }

        protected void ShowTheNewDice(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(((Label)sender).Tag);
            dices[index].Roll();
            ((Label)sender).Text = dices[index].Value.ToString();
        }
    }
}
