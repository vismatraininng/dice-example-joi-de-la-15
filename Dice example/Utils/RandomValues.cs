﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dice_example
{
    class RandomValues
    {

        static Random randomValue = new Random();

        internal static int NewDiceValue()
        {
            return randomValue.Next(1, 7);
        }

        //public static Card NewCard() 
        //{
        //    Card card = new Card();
        //    card.Value =  randomValue.Next(1, 13);
        //    card.Color =  randomValue.Next(1, 4);
        //}
    }
}
